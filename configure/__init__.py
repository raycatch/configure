import os
import re
import sys
import abc

import yaml
import copy
import codecs
import argparse
import functools
import itertools
import collections
import configparser

APPEND_CONFIG_SOURCE = -1


def singleton(class_):
    # From: http://stackoverflow.com/questions/6760685/creating-a-singleton-in-python
    instances = {}

    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]

    return getinstance


def walk(node, keys_list=None):
    # http://stackoverflow.com/questions/12507206/
    if keys_list is None:
        keys_list = []
    for key, item in node.items():
        if isinstance(item, dict):
            for result in walk(item, keys_list + [key]):
                yield result
        else:
            yield keys_list + [key], item


def recursive_get(node, key_list, default=None):
    for key in key_list:
        node = node.get(key, default)
        if node == default:
            break
    return node


def recursive_set(node, key_list, value):
    for key in key_list[:-1]:
        node = node.setdefault(key, {})
    node[key_list[-1]] = value


def flatten(dictionary, depth=1):
    """Flattens a dictionary. a negative depth value means full flatten.

    >>> flatten({1:{2:3}, 4:5})
    {2:3, 4:5}
    >>> flatten({1:{2:{3:3}}, 4:5})
    {2: {3: 3}, 4: 5}
    >>> flatten({1:{2:{3:3}}, 4:5}, 2)
    {3: 3, 4: 5}
    """
    dictionary = copy.deepcopy(dictionary)
    if depth < 0:
        iterations = itertools.count()  # range(inf)
    else:
        iterations = range(depth)
    for _ in iterations:
        previous_dictionary = copy.deepcopy(dictionary)
        for section_name, section in list(dictionary.items()):
            if not isinstance(section, dict):
                continue
            section = dictionary[section_name]
            for key in list(section.keys()):
                if key in dictionary:
                    raise KeyError("Argument already exists: '%s'. Can not flatten." % key)
                dictionary[key] = section.pop(key)
            dictionary.pop(section_name)
        if dictionary == previous_dictionary:
            break
    return dictionary


def return_value(key, value, config=None):
    return value


def return_key(key, value, config=None):
    return key


def apply_recursively(original, key_function=return_key, value_function=return_value):
    config = {}
    for key, value in original.items():
        if isinstance(value, collections.Mapping):
            config[key_function(key, value, config=original)] = apply_recursively(value, key_function, value_function)
        else:
            config[key_function(key, value, config=original)] = value_function(key, value, config=original)
    return config


def lower_key(key, value, config=None):
    return key.lower()


def lower(config_so_far, raw_config):
    return apply_recursively(raw_config, key_function=lower_key)


separators = re.compile(r'[^a-zA-Z0-9]')
def replace_separators(key, value, config=None):
    return separators.sub('_', key)


def replace_sep_to_underscore(config_so_far, raw_config):
    return apply_recursively(raw_config, key_function=replace_separators)


def delimit_config(config_so_far, raw_config, delimiter):
    config_so_far = None  # Unused parameter
    config = {}
    for key, value in raw_config.items():
        path_parts = key.split(delimiter)
        current_config = config
        for path_part in path_parts[:-1]:
            current_config = current_config.setdefault(path_part, {})
        if isinstance(value, collections.Mapping):
            current_config[path_parts[-1]] = delimit_config(config_so_far, value, delimiter)
        else:
            current_config[path_parts[-1]] = value

    return config


delimit_double_underscore = functools.partial(delimit_config, delimiter='__')


def recursive_update(original, update):
    for key, value in update.items():
        if isinstance(value, collections.Mapping):
            updated_value = recursive_update(original.get(key, {}), value)
            original[key] = updated_value
        else:
            original[key] = value
    return original  # Now updated.


class ConfigMap(object):
    def __init__(self, whitelist=None, blacklist=None, mandatory_arguments=None, translation_table=None):
        self.whitelist = whitelist
        self.blacklist = blacklist
        self.mandatory_arguments = mandatory_arguments
        self.translation_table = translation_table

    def apply_whitelist(self, config):
        if self.whitelist is None:
            return config
        filtered_config = {}
        for allowed_item in self.whitelist:
            if allowed_item in config.keys():
                filtered_config[allowed_item] = config[allowed_item]
        return filtered_config

    def apply_blacklist(self, config):
        if self.blacklist is None:
            return config
        filtered_config = config.copy()
        for forbidden_item in self.blacklist:
            if forbidden_item in filtered_config.keys():
                filtered_config.pop(forbidden_item)
        return filtered_config

    def verify_mandatory_arguments(self, config):
        if self.mandatory_arguments is None:
            return config
        for config_option in self.mandatory_arguments:
            if config_option not in config.keys() or config[config_option] is None:
                raise configparser.NoSectionError("Config option '%s' not found." % config_option)
        return config

    def translate_arguments(self, config):
        translation_list = []
        if self.translation_table is None:
            return config
        # Convert the translation dictionary to a pair of ((key1, key2, key3), value) == (route, translate_to)
        for keys_list, config_name in walk(self.translation_table):
            translation_list.append((keys_list, config_name))
        # Sort by the route length descending to allow popping.
        translation_list.sort(key=lambda thing: -len(thing[0]))

        for argument_keys, config_name in translation_list:
            try:
                parent_node = None
                current_node = config
                for key in argument_keys[:-1]:
                    parent_node = current_node
                    current_node = current_node[key]
                config[config_name] = current_node.pop(argument_keys[-1])
                if len(current_node.keys()) == 0 and parent_node is not None:
                    parent_node.pop(key)
            except KeyError:
                # Skip non-present arguments.
                continue

        return config

    def apply(self, config):
        if self.whitelist is not None:
            config = self.apply_whitelist(config)
        if self.blacklist is not None:
            config = self.apply_blacklist(config)
        if self.mandatory_arguments is not None:
            config = self.verify_mandatory_arguments(config)
        if self.translation_table is not None:
            config = self.translate_arguments(config)
        return config


class ConfigSource(object):
    def __init__(self):
        self.map = None

    @abc.abstractmethod
    def _get_config(self, config_so_far):
        """Subclasses override this! not get_config."""
        pass

    def get_config(self, config_so_far=None, *args, **kwargs):
        if config_so_far is None:
            config_so_far = {}
        config = self._get_config(config_so_far, *args, **kwargs)
        if self.map is not None:
            config = self.map.apply(config)
        return config

    def _on_config_manager_association(self, config_manager):
        """A hook called when this ConfigSource is associated to a ConfigManager."""
        pass

    def _on_config_manager_disassociation(self, config_manager):
        """A hook called when this ConfigSource is removed from a ConfigManager."""
        pass


class Hardcoded(ConfigSource):
    def __init__(self, **config_arguements):
        self.config = config_arguements
        super(Hardcoded, self).__init__()

    def _get_config(self, config_so_far):
        return self.config


class ConfigManager(ConfigSource):
    def __init__(self, transformations=(delimit_double_underscore, lower, replace_sep_to_underscore)):
        self.transformations = []
        for transformation in transformations:
            self.transformations.append(transformation)
        self._set_up_empty_configuration()
        self.resolving = False
        super(ConfigManager, self).__init__()

    def _set_up_empty_configuration(self):
        self.__config_sources = []
        self.__append_config_sources = []
        self.__arguments_config_sources_updated_with_proxy = []
        self._state_changed = True
        self.has_arguments_config_source = False
        self.args = None
        self.map = None
        self.arguments_proxy = ArgumentsProxy()
        self._config = None

    def add_argument(self, *args, **kwargs):
        self.arguments_proxy.add_argument(*args, **kwargs)
        self._state_changed = True

    def clear_config_options(self):
        return self._set_up_empty_configuration()

    def _get_config(self, config_so_far):
        # Make sure argument parsing is configured.
        if self.arguments_proxy.has_arguemnts() and not self.has_argument_parser():
            self.add_config_source(Arguments(), 0)
        for source in self.__append_config_sources:
            self.__config_sources.append(source)
        self._state_changed = True
        config = config_so_far
        while self._state_changed:
            self._state_changed = False
            for config_source in self.__config_sources:
                if (isinstance(config_source, Arguments) and
                    config_source not in self.__arguments_config_sources_updated_with_proxy
                        ):
                    config_source.update_arguments(self.arguments_proxy)
                    self.__arguments_config_sources_updated_with_proxy.append(config_source)
                    config_results = config_source.get_config(config, args=self.args)
                elif isinstance(config_source, dict):
                    config_results = copy.copy(config_source)
                else:
                    config_results = config_source.get_config(config)
                for transformation in self.transformations:
                    config_results = transformation(config_so_far, config_results)
                recursive_update(config, config_results)
                if self._state_changed:
                    break
        self._config = config
        self._state_changed = False
        return config

    def has_argument_parser(self):
        for source in self.__config_sources:
            if isinstance(source, Arguments):
                return True
        return False

    def _add_config_source(self, config_source, precedence=None):
        if hasattr(config_source, '_on_config_manager_association'):
            config_source._on_config_manager_association(self)
        if precedence is None:
            self.__config_sources.append(config_source)
        elif precedence == APPEND_CONFIG_SOURCE:
            self.__append_config_sources.append(config_source)
        else:
            self.__config_sources.insert(precedence, config_source)
        self._state_changed = True

    def _remove_config_source(self, config_source):
        if hasattr(config_source, '_on_config_manager_disassociation'):
            config_source._on_config_manager_disassociation(self)
        self.__config_sources.remove(config_source)
        self._state_changed = True

    def add_config_source(self, config_source, precedence=None):
        if isinstance(config_source, Arguments):
            self.has_arguments_config_source = True
        if isinstance(config_source, type):
            config_source = config_source()
        self._add_config_source(config_source, precedence)

    add_source = add_config_source

    def remove_config_source(self, config_source):
        self._remove_config_source(config_source)

    def get_config_sources(self):
        """The returned config sources are changable, but not removable."""
        return tuple(self.__config_sources)

    @property
    def config(self):
        if self._config is None or self._state_changed == True:
            self.get_config()
            self._state_changed = False
        return self._config

    def __getitem__(self, item):
        try:
            if not self.resolving:
                self.resolving = True
                self.get_config()
            return self.config[item]
        except KeyError:
            return Proxy(self, [item])

    def get(self, k, d=None):
        value = self.config[k]
        if isinstance(value, Proxy):
            value.default = d
        return value


@singleton
class DefaultConfigManager(ConfigManager):
    pass


class FileConfig(ConfigSource):
    """If flatten is set, the arguments will be stripped off their section."""
    default_section_name = u'DEFAULT'

    def __init__(self, file_path, flatten=False, convert_empty_to_none=False, encoding='utf8', flatten_depth=-1):
        self.config_parser = configparser.RawConfigParser()
        # Don't convert options to lower case.
        self.config_parser.optionxform = str
        self.config_path = file_path
        self.flatten = flatten
        self.flatten_depth = flatten_depth
        self.convert_empty_to_none = convert_empty_to_none
        self.encoding = encoding
        super(FileConfig, self).__init__()

    def _convert_to_dictionary(self, complete_config_parser):
        result = {}
        for key, value in complete_config_parser[self.default_section_name].items():
            result[key] = value
        for section_name, section_object in complete_config_parser.items():
            if section_name == self.default_section_name:
                continue
            result[section_name] = {}
            for key, value in section_object.items():
                if key not in complete_config_parser[self.default_section_name].keys():
                    result[section_name][key] = value
                else:
                    # Key has a default value. Ignore it if it is the same.
                    if value == complete_config_parser[self.default_section_name][key]:
                        continue
                    else:
                        result[section_name][key] = value
        return result

    def _get_config(self, config_so_far):
        self.config_parser.read_file(codecs.open(self.config_path, 'r', self.encoding))
        config = self._convert_to_dictionary(self.config_parser)
        if self.flatten:
            config = flatten(config, depth=self.flatten_depth)
        if self.convert_empty_to_none:
            for keys_list, value in walk(config):
                current_node = config
                for key in keys_list[:-1]:
                    current_node = current_node[key]
                key = keys_list[-1]
                value = current_node[key]
                if value == "":
                    current_node[key] = None
        return config


class YamlFile(ConfigSource):
    def __init__(self, file_path, flatten=False, encoding='utf8', flatten_depth=-1):
        """If flatten is set, the arguments will be stripped off their section."""
        self.config_path = file_path
        self.flatten = flatten
        self.flatten_depth = flatten_depth
        self.encoding = encoding
        super(YamlFile, self).__init__()

    def _get_config(self, config_so_far):
        config = yaml.load(codecs.open(self.config_path, 'r', self.encoding))
        if self.flatten:
            config = flatten(config, self.flatten_depth)
        return config


class DynamicFileConfig(ConfigSource):
    def __init__(self, config_location, config_source_class=FileConfig, *args, **kwargs):
        """config_location is an ordered collection of keys."""
        if isinstance(config_location, str):
            self.config_location = (config_location,)
        else:
            self.config_location = config_location
        self.config_source_class = config_source_class
        self.config_source_args = args
        self.config_source_kwargs = kwargs
        super(DynamicFileConfig, self).__init__()

    def _get_config(self, config_so_far):
        config_path_holder = config_so_far[self.config_location[0]]
        for key in self.config_location[1:]:
            config_path_holder = config_path_holder[key]
        self.config_path = config_path_holder
        config_engine = self.config_source_class(
            self.config_path, *self.config_source_args, ** self.config_source_kwargs)
        return config_engine._get_config(config_so_far)


class ArgumentParser(argparse.ArgumentParser):
    def parse_args(self, args=None, namespace=None):
        # Silences unknown arguments error.
        args, argv = self.parse_known_args(args, namespace)
        return args


class Arguments(ConfigSource):
    def __init__(self):
        self.parser = ArgumentParser()
        self.overrides = {'store_true': [], 'store_false': []}
        super(Arguments, self).__init__()

    def add_argument(self, *args, **kwargs):
        # Hack to bypass config default behaviour
        action = kwargs.get('action', None)
        if action in self.overrides.keys():
            self.overrides[action].append((args, kwargs))
        else:
            return self.parser.add_argument(*args, **kwargs)

    def _get_config(self, config_so_far, args=None):
        if args is None:
            args = sys.argv
        args, unparsed = self.parser.parse_known_args(args=args)
        raw_config = vars(args)
        config = {}
        for key, value in raw_config.items():
            # Ignore none values - they mean the argument was unset.
            if value is None:
                continue
            config[key] = value

        for action, arguments in self.overrides.items():
            for arg in arguments:
                overriden_method = getattr(self, action)
                key, value = overriden_method(arg, unparsed)
                if key is not None:
                    config[key] = value
        return config

    def store_true(self, argument_parameters, argv):
        argument = argument_parameters[0][0]
        if argument in argv:
            return argument.lstrip('-'), True
        return None, None

    def store_false(self, argument_parameters, argv):
        argument = argument_parameters[0][0]
        if argument in argv:
            return argument.lstrip('-'), False
        return None, None

    def update_arguments(self, another_arguemnts):
        for argument_args, argument_kwargs in another_arguemnts.arguments:
            self.add_argument(*argument_args, **argument_kwargs)


class ArgumentsProxy(Arguments):
    def __init__(self, *args, **kwargs):
        self.arguments = []
        super(ArgumentsProxy, self).__init__(*args, **kwargs)

    def add_argument(self, *args, **kwargs):
        self.arguments.append((args, kwargs))
        return None

    def _get_config(self, config_so_far, args=None):
        return {}

    def clear_arguments(self):
        self.arguments = []

    def has_arguemnts(self):
        return len(self.arguments) > 0


class EnvironmentVariables(ConfigSource):
    def _get_config(self, config_so_far):
        config = {}
        for key, value in os.environ.items():
            key = key.lower().replace('-', '_')
            lower_value = value.lower()
            if lower_value == 'true':
                value = True
            elif lower_value == 'false':
                value = False
            config[key] = value
        return config


class UpdateOnly(ConfigSource):
    def __init__(self, config_source):
        super(UpdateOnly, self).__init__()
        self.config_source = config_source

    def _get_config(self, config_so_far):
        new_config = self.config_source._get_config(copy.deepcopy(config_so_far))
        for key_list, item in walk(config_so_far):
            # Check if the item is present in the current config. Update it if so.
            new_option = recursive_get(new_config, key_list, KeyError())
            if not isinstance(new_option, KeyError):
                recursive_set(config_so_far, key_list, new_option)
        return config_so_far


class Proxy(object):
    def __init__(self, configuration, path, default=None):
        self.configuration = configuration
        self.path = path
        self.default_value = default
        self.use_default = False

    def set_default(self, value):
        self.use_default = True
        self.default_value = value

    def resolve(self):
        try:
            current_config = self.configuration
            for part in self.path:
                current_config = current_config[part]
            return current_config
        except KeyError:
            if self.use_default:
                return self.default_value
            else:
                raise

    @property
    def default(self):
        if self.use_default:
            return self.default_value
        else:
            raise ValueError("No default set.")

    @default.setter
    def default(self, value):
        self.set_default(value)

    @default.deleter
    def default(self):
        self.use_default = False
        self.default_value = None


class Dynamic(ConfigSource):
    """Huristic approach to file configuration."""
    extensions = {
        '.yaml': YamlFile
    }

    def __init__(self, file_name, *args, **kwargs):
        self.file_name = file_name
        self.config_source = None
        super(Dynamic, self).__init__()

    def _get_config(self, config_so_far):
        if self.file_name is None:
            return {}
        if isinstance(self.file_name, Proxy):
            file_name = self.file_name.resolve()
        else:
            file_name = self.file_name
        ext = os.path.splitext(file_name)[1]
        config_class = None
        if ext in self.extensions.keys():
            config_class = self.extensions[ext]
        else:
            raise NotImplementedError("Unable to determine file type.")
        self.config_source = config_class(file_name)
        return self.config_source.get_config(config_so_far)


def get_config_manager():
    return DefaultConfigManager()


default_setup = get_config_manager()
config_manager = get_config_manager()

# coding=utf-8

import tempfile
import unittest
import sys

import yaml

from context import configure
from configure import Hardcoded


class EventRecorder(configure.ConfigSource):
    events = []

    def __init__(self, event):
        self.event = event
        super(EventRecorder, self).__init__()

    def _get_config(self, config_so_far):
        EventRecorder.events.append(self.event)
        return config_so_far

    @classmethod
    def clear_events(cls):
        cls.events = []


class ConfigureTest(unittest.TestCase):
    def setUp(self):
        self.config_manager = configure.get_config_manager()
        self.config_manager.clear_config_options()


class UtilityTest(unittest.TestCase):
    pass


class TestConfigManager(ConfigureTest):
    C1_CONTENT = """
    [DEFAULT]
    a1 = 1
    a2 = 2
    """
    C2_CONTENT = """
    [DEFAULT]
    a1 = 11
    a3 = 3
    a4 = 4
    a5 = 5
    [s2]
    a2 = 22
    """
    C3_CONTENT = """
    [DEFAULT]
    a1 = 111
    a4 = 44
    [s2]
    a4 = 444
    """
    configs = [C1_CONTENT, C2_CONTENT, C3_CONTENT]
    config_paths = [None for c in configs]
    temp_files = []
    args = ("a5", "3")
    expected_result = {"a1": "111", "a2": "22", "a3": "3", "a4": "44", "a5": "3", "s2": {"a4": "444"}, "a6": "6"}

    def setUp(self):
        EventRecorder.clear_events()
        super(TestConfigManager, self).setUp()

    def write_conf_files(self):
        for index, file_content in enumerate(self.configs):
            file = tempfile.NamedTemporaryFile(delete=False)
            file.write(file_content)
            self.config_paths[index] = file.name
            self.temp_files.append(file.name)
            file.close()

    def test_configure(self):
        self.write_conf_files()
        c_m = configure.get_config_manager()
        for index, path in enumerate(self.config_paths):
            config_source = configure.FileConfig(path)
            if index == 1:
                # Flatten 2 for this test.
                config_source.flatten = True
            c_m.add_config_source(config_source)
        arguments = configure.Arguments()
        arguments.add_argument('a5')
        arguments.add_argument('--a6')
        arguments.add_argument('--a7')
        c_m.add_config_source(arguments)
        c_m.args = ("3", "--a6", "6")

        config = c_m.get_config()

        self.assertEqual(config, self.expected_result)

    def test_append_config_source(self):
        c_m = configure.get_config_manager()
        config_1 = EventRecorder(1)
        config_2 = EventRecorder(2)
        c_m.add_config_source(config_1, configure.APPEND_CONFIG_SOURCE)
        c_m.add_config_source(config_2)
        c_m.get_config()
        self.assertEqual(EventRecorder.events, [2, 1])


class TestManagerIsSingleton(ConfigureTest):
    def test_manger_is_singleton(self):
        manager1 = configure.get_config_manager()
        manager2 = configure.get_config_manager()
        self.assertEqual(manager1, manager2)


class TestDefaultArgumentParse(ConfigureTest):
    def test_default_argument_parse(self):
        config_manager = configure.get_config_manager()
        config_manager.add_argument('--data_sources', default="default")
        config_manager.args = ["--data_sources", "non_default"]
        self.assertEqual(config_manager.get_config()["data_sources"], "non_default")


class TestDynamicFileConfig(ConfigureTest):
    def setUp(self):
        super(TestDynamicFileConfig, self).setUp()
        self.previous_arguments = sys.argv
        open('test_config_file.cfg', 'w').write('[main]\n'
                                           'a = 1')
        sys.argv = ["tests.py", "--file", "test_config_file.cfg"]

    def tearDown(self):
        sys.argv = self.previous_arguments
        import os
        try:
            os.remove('test_config_file.cfg')
        except:
            pass
        super(TestDynamicFileConfig, self).tearDown()

    def test_dynamic_file_from_argument(self):
        config_manager = configure.get_config_manager()
        config_manager.add_argument('--file', default="dafault_file")
        config_manager.add_config_source(configure.DynamicFileConfig("file", configure.FileConfig))
        config = config_manager.get_config()
        self.assertEqual(config, {'main': {'a': '1'}, 'file': 'test_config_file.cfg'})


class TestMultipleArgumentParsers(ConfigureTest):
    def test_multiple_argument_parsers(self):
        config_manager = configure.get_config_manager()
        args = configure.Arguments()
        args.add_argument('--a', default='1')
        config_manager.add_config_source(args)
        other = Hardcoded(a='2', b='2')
        config_manager.add_config_source(other)
        config_manager.add_config_source(args)
        config = config_manager.get_config()
        self.assertEqual(config, {'a': '1', 'b': '2'})

    def test_proxy_with_multiple_arguments_parsers(self):
        config_manager = configure.get_config_manager()
        config_manager.add_argument('--c', default='3')
        args = configure.Arguments()
        args.add_argument('--a', default='1')
        config_manager.add_config_source(args)
        other = Hardcoded(a='2', b='2')
        config_manager.add_config_source(other)
        config_manager.add_config_source(args)
        config = config_manager.get_config()
        self.assertEqual(config, {'a': '1', 'b': '2', 'c': '3'})


class TestMapTranslation(ConfigureTest):
    def setUp(self):
        super(TestMapTranslation, self).setUp()
        self.config_manager.add_config_source(Hardcoded(a='1', b={'c':'3', 'd':'4', 'e':{'f': '5'}}))

    def test_nested_translate(self):
        self.config_manager.map = configure.ConfigMap(translation_table={'b': {'d': 'd', 'e': {'f': 'f'}}})
        config = self.config_manager.get_config()
        self.assertEqual(config, {'a': '1', 'b': {'c': '3'}, 'd': '4', 'f': '5'})

    def test_trivial_translate(self):
        self.config_manager.map = configure.ConfigMap(translation_table={'a': 'h'})
        config = self.config_manager.get_config()
        self.assertEqual(config, {'h': '1', 'b': {'c': '3', 'd': '4', 'e': {'f': '5'}}})


class TestFileConfig(ConfigureTest):
    def test_case_sensitivity(self):
        with tempfile.NamedTemporaryFile(delete=False) as file:
            file.write("[main]\nAbC dE = 123")
            config_path = file.name
            file_config = configure.FileConfig(config_path, flatten=True)
        config = file_config.get_config()
        self.assertEqual(config, {"AbC dE": "123"})

    def test_convert_empty_to_none(self):
        with tempfile.NamedTemporaryFile(delete=False) as file:
            file.write("[main]\na = \nb = 1")
            config_path = file.name
            file_config = configure.FileConfig(config_path, convert_empty_to_none=True)
        config = file_config.get_config()
        self.assertEqual(config, {"main": {"a": None, "b": "1"}})

    def test_unicode_file(self):
        with tempfile.NamedTemporaryFile(delete=False) as file:
            file.write(u"[main]\na = WEB’log".encode('utf8'))
            config_path = file.name
            file_config = configure.FileConfig(config_path, convert_empty_to_none=True)
        config = file_config.get_config()
        self.assertEqual(config, {"main": {"a": u'WEB’log'}})


class TestYamlFile(ConfigureTest):
    configuration = {"main": {"a": u'WEB’log', "b": {"c": "d"}}}
    configuration_flattened = {"a": u'WEB’log', "b": {"c": "d"}}
    configuration_flattened_level_2 = {"a": u'WEB’log', "c": "d"}

    def test_unicode_file(self):
        with tempfile.NamedTemporaryFile(delete=False) as file:
            yaml.dump(self.configuration, file)
            config_path = file.name
            yaml_config = configure.YamlFile(config_path)
        config = yaml_config.get_config()
        self.assertEqual(config, self.configuration)

    def test_flatten_file(self):
        with tempfile.NamedTemporaryFile(delete=False) as file:
            yaml.dump(self.configuration, file)
            config_path = file.name
            yaml_config = configure.YamlFile(config_path, flatten=True, flatten_depth=1)
            yaml_config_2 = configure.YamlFile(config_path, flatten=True, flatten_depth=2)
            yaml_config_3 = configure.YamlFile(config_path, flatten=True, flatten_depth=-1)
        config = yaml_config.get_config()
        self.assertEqual(config, self.configuration_flattened)
        config_2 = yaml_config_2.get_config()
        self.assertEqual(config_2, self.configuration_flattened_level_2)
        config_3 = yaml_config_3.get_config()
        self.assertEqual(config_3, self.configuration_flattened_level_2)


class TestFlatten(UtilityTest):
    configuration = {"main": {"a": u'WEB’log', "b": {"c": "d"}}}
    depth_1 = {"a": u'WEB’log', "b": {"c": "d"}}
    depth_2 = {"a": u'WEB’log', "c": "d"}

    def test_flatten_file(self):
        depth_0 = configure.flatten(self.configuration, depth=0)
        self.assertEqual(depth_0, self.configuration)
        depth_1 = configure.flatten(self.configuration, depth=1)
        self.assertEqual(depth_1, self.depth_1)
        depth_2 = configure.flatten(self.configuration, depth=2)
        self.assertEqual(depth_2, self.depth_2)
        depth_full = configure.flatten(self.configuration, depth=-1)
        self.assertEqual(depth_full, self.depth_2)


class TestDefaultTransformations(ConfigureTest):
    conf_1 = {"CaTeGoRy 1__category_2__Just.Another,Thing\wiTh-lOng_/name_": "Value with.chars,'/\\][\""}
    conf_1_expected = {
        "category_1": {
            "category_2": {
                "just_another_thing_with_long__name_": "Value with.chars,'/\\][\""}}}

    def test_default_transformations(self):
        self.config_manager.add_config_source(Hardcoded(**self.conf_1))
        self.assertEqual(self.conf_1_expected, self.config_manager.config)


if '__main__' == __name__:
    unittest.main()

# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='configure',
    version='0.3.9',  # Major.Minor.Patch
    # Major: API might not be consistent between major revisions. (although this is not expected nor wanted)
    # Minor: Redundant and unused APIs might change and be extended between patches.
    # Patch: New API and minor changes that don't effect the API are introduced between minor versions.
    description='The configuration manager of Raycatch',
    long_description=readme,
    author='Itay Elbirt',
    author_email='tulyhamudi@gmail.com',
    url='https://bitbucket.com/raycatch/configure',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
